# Threshold

IoT(BLE) Gateway for https://nrfcloud.com

## Execute

### Manually

- `DEBUG=AWSGateway:debug,AWSGateway:info,AWSGateway:error NRF_CONNECT_USERNAME=myemail NRF_CONNECT_PASSWORD='password' node index`

### Service

- `./install-services.sh`
- `sudo systemctl restart threshold.service`
- `journalctl -fu threshold.service`

### BalenaOS

- TODO: write docker file

## Based on:

- https://github.com/nRFCloud/gateway-common
- https://github.com/nRFCloud/gateway-cordova
- https://github.com/nRFCloud/beacon-utilities
