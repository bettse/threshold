global.fetch = require('node-fetch');
const AWS = require('aws-sdk');
const {
  AuthenticationDetails,
  CognitoUserPool,
  CognitoUser,
} = require('amazon-cognito-identity-js');
const {device, thingShadow} = require('aws-iot-device-sdk');
const {GatewayAWS} = require('nrfcloud-gateway-common');

const {
  ThresholdAdapterDriverFactory,
  ThresholdAdapter,
  ThresholdAdapterDriver,
} = require('./ThresholdAdapter');
const fsAdapter = require('./fsAdapter');
const {IrisRestApiClient} = require('./IrisRestApiClient');

const GATEWAY_VERSION = require('./package.json').version;
const caCert = fsAdapter.readFile('./AmazonRootCA1.pem');

async function login(Username, Password) {
  const authenticationData = {Username, Password};
  const authenticationDetails = new AuthenticationDetails(authenticationData);
  const poolData = {
    UserPoolId: 'us-east-1_fdiBa7JSO',
    ClientId: '2p40shpt1ru5gerbip9limhm15',
  };
  const userPool = new CognitoUserPool(poolData);
  const userData = {
    Username: authenticationData.Username,
    Pool: userPool,
  };
  const cognitoUser = new CognitoUser(userData);
  const result = await new Promise((resolve, reject) => {
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: resolve,
      onFailure: reject,
    });
  });

  const idToken = result.getIdToken().getJwtToken();
  AWS.config.region = 'us-east-1';

  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-1:c00e1327-dfc2-4aa7-a484-8ca366d11a68',
    Logins: {
      'cognito-idp.us-east-1.amazonaws.com/us-east-1_fdiBa7JSO': idToken,
    },
  });

  //await AWS.config.credentials.getPromise();
  //refreshes credentials using AWS.CognitoIdentity.getCredentialsForIdentity()
  await AWS.config.credentials.refreshPromise();

  return AWS.config.credentials.data.Credentials;
}

async function connectGateway(credentials) {
  const logger = {
    debug: require('debug')('AWSGateway:debug'),
    info: require('debug')('AWSGateway:info'),
    error: require('debug')('AWSGateway:error'),
  };

  const gateway = new GatewayAWS(
    'config/gateway.json',
    'threshold',
    fsAdapter,
    GATEWAY_VERSION,
    logger,
  );
  gateway.setConnectOptions({
    accessKey: credentials.AccessKeyId,
    accessKeyId: credentials.AccessKeyId,
    secretKey: credentials.SecretKey,
    sessionToken: credentials.SessionToken,
    host: 'a2n7tk1kp18wix-ats.iot.us-east-1.amazonaws.com',
    debug: false,
  });
  gateway.setAdapterDriverFactory(new ThresholdAdapterDriverFactory());
  await gateway.init();
  await gateway.setAdapter('threshold');

  if (!gateway.isRegistered()) {
    const client = new IrisRestApiClient({
      accessKey: credentials.AccessKeyId,
      secretKey: credentials.SecretKey,
      sessionToken: credentials.SessionToken,
    });

    await registerGateway(client, gateway);
    // await getGateways(client);
  }

  await gateway.start();

  return gateway;
}

async function getCurrentTenant(clientApi) {
  const tenantsGetResult = await getTenants(clientApi);
  if (
    tenantsGetResult.length &&
    tenantsGetResult[0] &&
    tenantsGetResult[0].id
  ) {
    return tenantsGetResult[0];
  }
  throw new Error('No tenant for user');
}

async function getTenants(clientApi = client) {
  return (await clientApi.tenantsGet({create: false})).data;
}

async function registerGateway(clientApi, gateway) {
  const tenant = await getCurrentTenant(clientApi);
  const tenantId = tenant.id;

  const gatewaysPostResult = (await clientApi.tenantsTenantIdGatewaysPost({
    tenantId,
  })).data;

  await gateway.setCredentials(
    tenantId,
    gatewaysPostResult.gatewayId,
    gatewaysPostResult.clientCert,
    gatewaysPostResult.privateKey,
    caCert,
  );
}

const {NRF_CONNECT_USERNAME, NRF_CONNECT_PASSWORD} = process.env;
login(NRF_CONNECT_USERNAME, NRF_CONNECT_PASSWORD)
  .then(connectGateway)
  .catch(error => {
    console.log('Error in main execution', error);
  });
