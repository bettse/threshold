const apigClientFactory = require('aws-api-gateway-client').default;

const invokeUrl = 'https://hnmr2uba55.execute-api.us-east-1.amazonaws.com/prod';

class IrisRestApiClient {
  constructor(credentials) {
    /*
    accessKey: 'ACCESS_KEY', // REQUIRED
    secretKey: 'SECRET_KEY', // REQUIRED
    sessionToken: 'SESSION_TOKEN', //OPTIONAL: If you are using temporary credentials you must include the session token
    */
    this.apigClient = apigClientFactory.newClient({
      invokeUrl,
      region: 'us-east-1',
      ...credentials,
    });
  }

  async tenantsGet(params) {
    const pathParams = {};
    const pathTemplate = '/tenants';
    const method = 'GET';
    const additionalParams = {
      headers: {},
      queryParams: {
        ...params,
      },
    };
    const body = {};

    return this.apigClient.invokeApi(
      pathParams,
      pathTemplate,
      method,
      additionalParams,
      body,
    );
  }

  async tenantsTenantIdGatewaysPost(params) {
    const pathParams = {...params};

    const pathTemplate = '/tenants/{tenantId}/gateways';
    const method = 'POST';
    const additionalParams = {
      headers: {},
      queryParams: {},
    };
    const body = {};

    return this.apigClient.invokeApi(
      pathParams,
      pathTemplate,
      method,
      additionalParams,
      body,
    );
  }
}

module.exports = {
  IrisRestApiClient,
};
