const {EventEmitter} = require('events');
const util = require('util');
const noble = require('@abandonware/noble');
const {
  CharacteristicProperties,
  ConnectTimedOutEvent,
  ConnectionCharacteristicValueChangedEvent,
  ConnectionDescriptorValueChangedEvent,
  ConnectionDownEvent,
  ConnectionErrorEvent,
  ConnectionScanParams,
  ConnectionUpEvent,
  ScanType,
} = require('nrfcloud-gateway-common');
const {isBeacon} = require('beacon-utilities');
const debug = require('debug')('ThresholdAdapterDriver');

class ThresholdAdapterDriverFactory extends EventEmitter {
  getAdapterByAdapterId(adapterId) {
    return new ThresholdAdapter(adapterId);
  }

  getAdapters() {
    return ['threshold'];
  }
}

class ThresholdAdapter {
  constructor(id) {
    this.id = id;
  }

  getAdapterDriver() {
    return new ThresholdAdapterDriver();
  }
}

const ADAPTER_ERROR = 'adapterError';
const ADAPTER_WARNING = 'adapterWarning';
const ADAPTER_STATE_CHANGE = 'adapterStateChange';
const CONNECTION_ERROR = 'connectionError';
const CONNECTION_UP = 'connectionUp';
const CONNECTION_DOWN = 'connectionDown';
const CONNECTION_SECURITY_REQUEST = 'connectionSecurityRequest';
const CONNECTION_SECURITY_PARAMETERS_REQUEST =
  'connectionSecurityParametersRequest';
const CONNECTION_AUTHENTICATION_STATUS = 'connectionAuthenticationStatus';
const CONNECTION_CHARACTERISTIC_VALUE_CHANGED = 'characteristicValueChanged';
const CONNECTION_DESCRIPTOR_VALUE_CHANGED = 'descriptorValueChanged';
const CONNECT_TIMEOUT = 'connectTimeout';
const CONNECT_CANCELED = 'connectCanceled';
const DEVICE_DISCOVERED = 'deviceDiscovered';
const DEVICE_UPDATE = 'deviceUpdate';

const BEACON_SCAN_INTERVAL = 60; //Time between beacon scans, in seconds
const BEACON_SCAN_DURATION = 2; //How long to scan for beacons, in seconds
const SCAN_RESULT_INTERVAL = 1; //How long to wait inbetween sending scan results, in MICROSECONDS!

//https://www.bluetooth.com/specifications/gatt/descriptors/
const CCCD = '2902';

class ThresholdAdapterDriver extends EventEmitter {
  constructor() {
    super();
    this.state = {
      available: false,
      bleEnabled: false,
      scanning: false,
      advertising: false,
      connecting: false,
    };

    this.watching = [];

    this.peripherals = {}; // Scanned noble peripherals
    this.devices = {}; // Connected noble peripherals
    this.services = {};
    this.characteristics = {};
    this.descriptors = {};

    this.beacons = {};
    this.scanParams = new ConnectionScanParams();

    this.watcherHolder = setInterval(async () => {
      await this.performWatches();
    }, BEACON_SCAN_INTERVAL * 1000); //watch every x seconds
  }

  debug() {
    return console.log(arguments);
  }

  getState() {
    return this.state;
  }

  /**
   * Set default security parameters
   */
  setDefaultSecurityParameters(securityParameters) {
    this.securityParameters = securityParameters;
  }

  open() {
    noble.on('stateChange', state => {
      if (state === 'poweredOn') {
        noble.on('discover', this.onDeviceDiscovered.bind(this));
        this.updateState({available: true, bleEnabled: true});
      }
    });
  }

  close() {
    this.updateState({
      available: false,
      bleEnabled: false,
      scanning: false,
      advertising: false,
      connecting: false,
    });
  }

  reset() {
    this.close() && this.open();
  }

  /**
   * Starts scanning for devices
   * @param active Active scanning
   * @param interval Scan interval
   * @param window Scan window
   * @param timeout Scan timeout
   * @param batch Batch up scan reports until scan has timed out
   * @param rssi RSSI value threshold, values lower than threadhold will be discarded
   * @param name Filter on advertised name
   * @param scanType Type of scan to do: 0 = Regular, 1 = Beacon
   */
  //startScan(active: boolean, interval: number, window: number, timeout?: number, batch?: boolean, rssi?: number, name?: string, scanType?: ScanType): Promise<void>;
  async startScan(
    active,
    interval,
    window,
    timeout,
    batch,
    rssi,
    name,
    scanType,
  ) {
    this.scanParams = {
      active,
      batch,
      rssi,
      name,
      scanType: scanType || ScanType.Regular,
    };

    noble.startScanning();
    this.updateState({scanning: true});
    return new Promise(resolve =>
      setTimeout(() => {
        this.stopScan();
        resolve();
      }, timeout * 1000),
    );
  }

  onDeviceDiscovered(peripheral) {
    const {active, batch, rssi, name, scanType} = this.scanParams;
    const {advertisement, address, addressType, rssi: prssi} = peripheral;
    const {
      localName,
      txPowerLevel,
      serviceUuids,
      manufacturerData,
      serviceData,
    } = advertisement;

    // Keep track locally of everything seen
    this.peripherals[address] = Object.assign(
      peripheral,
      this.peripherals[address],
    );

    // serviceData: {[key: string]: number[]};
    const formattedServiceData = serviceData.reduce((acc, service) => {
      const {uuid, data} = service;
      return Object.assign(acc, {[uuid]: Array.from(data)});
    }, {});

    // manufacturerData: {[key: number]: number[]};
    const formattedManufacturerData = manufacturerData && {
      [manufacturerData.readUInt16LE(0)]: Array.from(manufacturerData.slice(2)),
    };

    const advertisementData = {
      txPower: txPowerLevel,
      serviceUuids,
      localName,
      manufacturerData: formattedManufacturerData || {},
      serviceData: formattedServiceData,
    };

    const device = {
      address: {
        address,
        type: this.formatAddressType(addressType),
      },
      connected: false,
      name: localName,
      txPower: txPowerLevel,
      rssi: prssi,
      rssiLevel: prssi,
      advertisementData,
      time: new Date(),
    };

    if (rssi && prssi < rssi) {
      return;
    }

    if (name && localName !== name) {
      return;
    }

    // Update beacon data even on non-beacon scans
    if (isBeacon(advertisementData)) {
      if (this.watching.includes(address)) {
        this.beacons[address] = Object.assign(
          {},
          this.beacons[address],
          device,
          {id: address, statistics: {rssi: prssi}},
          {isBeacon: true},
        );
      }
    }

    // Ignore devices of the other scan mode
    if (isBeacon(advertisementData) !== (scanType === ScanType.Beacon)) {
      // console.log(isBeacon(advertisementData), scanType === ScanType.Beacon);
      return;
    }

    if (active) {
      this.emit(DEVICE_DISCOVERED, device);
    }
  }

  stopScan() {
    noble.stopScanning();
    this.updateState({scanning: false});
  }

  async connect(connection, connectOptions) {
    const {address, type} = connection;

    // Already connected
    if (this.devices[address]) {
      this.emit(CONNECTION_UP, new ConnectionUpEvent(connection));
      return connection;
    }

    try {
      this.updateState({connecting: true});
      if (!this.peripherals[address]) {
        const {scanParams} = connectOptions;
        const {active, interval, window, timeout} = scanParams;
        await this.startScan(active, interval, window, timeout);
      }

      const peripheral = this.peripherals[address];
      if (peripheral) {
        const connect = util.promisify(peripheral.connect.bind(peripheral));
        peripheral.once('connect', () => {
          this.devices[address] = peripheral;
          this.emit(CONNECTION_UP, new ConnectionUpEvent(connection));
        });

        peripheral.once('disconnect', () => {
          delete this.devices[address];
          this.emit(CONNECTION_DOWN, new ConnectionDownEvent(connection));
        });

        await connect();
        return connection;
      }
    } catch (error) {
      this.emit(CONNECTION_ERROR, new ConnectionErrorEvent(connection, error));
      throw error;
    } finally {
      this.updateState({connecting: false});
    }

    this.emit(CONNECT_TIMEOUT, new ConnectTimedOutEvent(connection));
    return Promise.reject('timeout');
  }

  async disconnect(connection) {
    const {address, type} = connection;
    const peripheral = this.devices[address];

    if (peripheral) {
      const disconnect = util.promisify(peripheral.disconnect.bind(peripheral));
      return await disconnect();
    }
    return Promise.resolve();
  }

  cancelConnect() {
    return Promise.resolve();
  }

  getConnections() {
    return Object.values(this.devices).map(device => {
      const {address, addressType} = device;
      return {address, type: this.formatAddressType(addressType)};
    });
  }

  getImpl() {
    return moble;
  }

  /**
   * Start authentication towards peer
   */
  //authenticate(connection: Address, securityParameters: ConnectionSecurityParams): Promise<void>;
  authenticate(connection, securityParameters) {
    return Promise.resolve();
  }

  /**
   * Reply to a security parameter request
   */
  //securityParametersReply(connection: Address, status: string, securityParameters: ConnectionSecurityParams): Promise<void>;
  securityParametersReply(connection, status, securityParameters) {
    return Promise.resolve();
  }

  //sendPasskey(connection: Address, keyType: string, key: string): Promise<void>;
  sendPasskey(connection, keyType, key) {
    return Promise.resolve();
  }

  async getServices(connection) {
    const {address, type} = connection;
    const peripheral = this.devices[address];
    if (!peripheral) {
      return Promise.reject(`No peripheral found for ${address}`);
    }

    const discoverServices = util.promisify(
      peripheral.discoverServices.bind(peripheral),
    );
    const services = await discoverServices([]);

    return await Promise.all(
      services.map(async service => {
        const {uuid, characteristics} = service;
        this.services[uuid] = service;

        const formattedService = {
          uuid,
          characteristics: [],
          path: uuid,
          //startHandle: number;
          //endHandle: number;
        };

        try {
          formattedService.characteristics = await this.getCharacteristics(
            connection,
            formattedService,
          );
        } catch (e) {}
        return formattedService;
      }),
    );
  }

  getAttributes(connection) {
    return this.getServices(connection);
  }

  // Format from noble's array to object
  formatCharacteristicProperties(properties) {
    const characteristicProperties = new CharacteristicProperties();
    properties.forEach(property => {
      switch (property) {
        case 'writeWithoutResponse':
          characteristicProperties['write_wo_resp'] = true;
          break;
        case 'authenticatedSignedWrites':
          characteristicProperties['auth_signed_wr'] = true;
          break;
        default:
          characteristicProperties[property] = true;
      }
    });
    return characteristicProperties;
  }

  formatAddressType(addressType) {
    switch (addressType) {
      case 'random':
        return 'randomStatic';
      case 'public':
      case 'unknown':
        return 'public';
      default:
        console.log(`Cannot format address type ${addressType}`);
        return addressType;
    }
    // Other types: 'randomPrivateResolvable' 'randomPrivateNonResolvable';
  }

  async getCharacteristics(connection, service) {
    const {address, type} = connection;
    const {uuid} = service;
    const thisService = this.services[uuid];
    if (!thisService) {
      return Promise.reject(`No service found for ${uuid}`);
    }
    const discoverCharacteristics = util.promisify(
      thisService.discoverCharacteristics.bind(thisService),
    );
    const characteristics = await discoverCharacteristics([]);

    return await Promise.all(
      characteristics.map(c => this.formatCharacteristic(c, thisService)),
    );
  }

  async formatCharacteristic(characteristic, service) {
    const {uuid, properties, value, descriptors} = characteristic;

    this.characteristics[uuid] = characteristic;

    const formattedCharacteristic = {
      uuid,
      properties: this.formatCharacteristicProperties(properties),
      value: value || [],
      descriptors: descriptors || [],
      path: `${service.uuid}/${uuid}`,
      //declarationHandle: number;
      //valueHandle: number;
    };

    if (!value && properties.includes('read')) {
      try {
        formattedCharacteristic.value = await this.readCharacteristicValue(
          null,
          formattedCharacteristic,
        );
      } catch (e) {
        console.log('readCharacteristicValue', e);
      }
    }

    if (!descriptors) {
      try {
        formattedCharacteristic.descriptors = await this.getDescriptors(
          null,
          formattedCharacteristic,
        );
      } catch (e) {
        console.log('getDescriptors', e);
      }
    }

    return formattedCharacteristic;
  }

  //getDescriptors(connection: Address, characteristic: Characteristic): Promise<Array<Descriptor>>;
  async getDescriptors(connection, characteristic) {
    const {uuid} = characteristic;
    const thisCharacteristic = this.characteristics[uuid];
    if (!thisCharacteristic) {
      Promise.reject(`No characteristic found for ${uuid}`);
    }
    const discoverDescriptors = util.promisify(
      thisCharacteristic.discoverDescriptors.bind(thisCharacteristic),
    );
    const descriptors = await discoverDescriptors();

    return await Promise.all(
      descriptors.map(d => this.formatDescriptor(d, characteristic)),
    );
  }

  async formatDescriptor(descriptor, characteristic) {
    const {uuid, value} = descriptor;
    this.descriptors[uuid] = descriptor;

    const formattedDescriptor = {
      uuid,
      value: value || [],
      path: `${characteristic.path}/${uuid}`,
      //handle: number;
    };
    try {
      formattedDescriptor.value = await this.readDescriptorValue(
        connection,
        formattedDescriptor,
      );
    } catch (e) {}
    return formattedDescriptor;
  }

  //readCharacteristicValue(connection: Address, characteristic: Characteristic): Promise<Array<number>>;
  async readCharacteristicValue(connection, characteristic) {
    const {uuid} = characteristic;
    const thisCharacteristic = this.characteristics[uuid];
    if (!thisCharacteristic) {
      return Promise.reject(`No characteristic found for ${uuid}`);
    }
    const read = util.promisify(
      thisCharacteristic.read.bind(thisCharacteristic),
    );
    const data = await read();
    return Array.from(data);
  }

  // writeCharacteristicValue(connection: Address, characteristic: Characteristic, characteristicValue: Array<number>, ack: boolean): Promise<void>;
  async writeCharacteristicValue(
    connection,
    characteristic,
    characteristicValue,
    ack,
  ) {
    const {address, type} = connection;
    const {uuid} = characteristic;
    // const service = characteristic.path.split('/')[0];
    const thisCharacteristic = this.characteristics[uuid];
    if (!thisCharacteristic) {
      return Promise.reject(`No characteristic found for ${uuid}`);
    }
    const data = Buffer.from(characteristicValue);
    const write = util.promisify(
      thisCharacteristic.write.bind(thisCharacteristic),
    );

    await write(data);
    characteristic.value = characteristicValue;

    this.emit(
      CONNECTION_CHARACTERISTIC_VALUE_CHANGED,
      new ConnectionCharacteristicValueChangedEvent(connection, characteristic),
    );

    return;
  }

  // readDescriptorValue(connection: Address, descriptor: Descriptor): Promise<Array<number>>;
  async readDescriptorValue(connection, descriptor) {
    const {address, type} = connection;
    const {uuid} = descriptor;
    const thisDescriptor = this.descriptors[uuid];
    if (!thisDescriptor) {
      return Promise.reject(`No descriptor found for ${uuid}`);
    }
    const readValue = util.promisify(
      thisDescriptor.readValue.bind(thisDescriptor),
    );
    const value = await readValue();
    return Array.from(value);
  }

  // writeDescriptorValue(connection: Address, descriptor: Descriptor, descriptorValue: Array<number>, ack: boolean): Promise<void>;
  async writeDescriptorValue(connection, descriptor, descriptorValue, ack) {
    const {address, type} = connection;
    const {uuid, path} = descriptor;

    // CCCD org.bluetooth.descriptor.gatt.client_characteristic_configuration
    if (uuid === CCCD) {
      const parts = path.split('/');
      const [serviceUuid, characteristicUuid, descriptorUuid] = parts;

      const thisCharacteristic = this.characteristics[characteristicUuid];
      if (!thisCharacteristic) {
        return Promise.reject(
          `No characteristic found for ${characteristicUuid}`,
        );
      }

      if (descriptorValue.length > 0 && descriptorValue[0]) {
        await this.subscribe(connection, thisCharacteristic, {
          uuid: serviceUuid,
        });
      } else {
        await this.unsubscribe(connection, thisCharacteristic);
      }

      descriptor.value = descriptorValue;
      this.emit(
        CONNECTION_DESCRIPTOR_VALUE_CHANGED,
        new ConnectionDescriptorValueChangedEvent(connection, descriptor),
      );

      return;
    }

    const thisDescriptor = this.descriptors[uuid];
    if (!thisDescriptor) {
      return Promise.reject(`No descriptor found for ${uuid}`);
    }
    const data = Buffer.from(descriptorValue);

    const writeValue = util.promisify(
      thisDescriptor.writeValue.bind(thisDescriptor),
    );
    await writeValue(data);
    descriptor.value = descriptorValue;
    this.emit(
      CONNECTION_DESCRIPTOR_VALUE_CHANGED,
      new ConnectionDescriptorValueChangedEvent(connection, descriptor),
    );
    return;
  }

  /**
   * Subscribe to a characteristic
   * @param connection Address - {address, type}
   * @param characteristic Noble characteristic
   * @param service Object - has service uuid
   */
  async subscribe(connection, characteristic, service) {
    const subscribe = util.promisify(
      characteristic.subscribe.bind(characteristic),
    );

    characteristic.on('data', async data => {
      // Setting the value prevents formattedCharacteristic from doing a read
      characteristic.value = Array.from(data);
      const formattedCharacteristic = await this.formatCharacteristic(
        characteristic,
        service,
      );

      this.emit(
        CONNECTION_CHARACTERISTIC_VALUE_CHANGED,
        new ConnectionCharacteristicValueChangedEvent(
          connection,
          formattedCharacteristic,
        ),
      );
    });
    return await subscribe();
  }

  /**
   * Unsubscribe from a characteristic
   * @param connection Address - {address, type}
   * @param characteristic Noble characteristic
   */
  async unsubscribe(connection, characteristic) {
    const unsubscribe = util.promisify(
      characteristic.unsubscribe.bind(characteristic),
    );
    return await unsubscribe();
  }

  //watchDevices(connections: string[]): Promise<string[]>;
  async watchDevices(connections) {
    this.watching = connections;
    this.watching.forEach(address => {
      //this.updateBeacon(address)
    });
    this.performWatches();
    return connections;
  }

  //unwatchDevices(connections: string[]): Promise<string[]>;
  async unwatchDevices(connections) {
    this.watching = this.watching.filter(a => !connections.includes(a));
    return connections;
  }

  /*
  on(event: 'deviceDiscovered', listener: (discoveredDevice: DeviceDiscovered | null, timeout?: boolean) => void): this;

  on(event: 'devicesDiscovered', listener: (dicoveredDevices: Array<DeviceDiscovered>) => void): this;

  on(event: 'adapterStateChange', listener: (stateChanged: AdapterState) => void): this;

  on(event: 'adapterError', listener: (error: AdapterError) => void): this;

  on(event: 'adapterWarning', listener: (warning: AdapterWarning) => void): this;

  on(event: 'connectionDown', handler: (n: ConnectionDownEvent) => void): void;

  on(event: 'connectionUp', handler: (n: ConnectionUpEvent) => void): void;

  on(event: 'connectTimedOut', handler: (n: ConnectTimedOutEvent) => void): void;

  on(event: 'connectionError', handler: (n: ConnectionErrorEvent) => void): void;

  on(event: 'connectCanceled', handler: (n: ConnectCanceledEvent) => void): void;

  on(event: 'connectionSecurityRequest', handler: (n: ConnectionSecurityRequestEvent) => void): void;

  on(event: 'connectionSecurityParametersRequest', handler: (n: ConnectionSecurityParametersRequestEvent) => void): void;

  on(event: 'connectionAuthenticationStatus', handler: (n: ConnectionAuthenticationStatusEvent) => void): void;

  on(event: 'characteristicValueChanged', handler: (n: ConnectionCharacteristicValueChangedEvent) => void): void;

  on(event: 'descriptorValueChanged', handler: (n: ConnectionDescriptorValueChangedEvent) => void): void;

  on(event: 'errorEvent', handler: (n: ErrorEvent) => void): void;
  */

  async performWatches() {
    const watchString = `performing watches: ${this.watching &&
      this.watching.length} device${
      this.watching && this.watching.length === 1 ? '' : 's'
    }`;
    debug(watchString);
    if (!this.watching || this.watching.length < 1) {
      return; //nothing to watch
    }

    if (this.state.scanning) {
      return;
    }

    await this.startScan(true, 100, 100, BEACON_SCAN_DURATION, ScanType.Beacon);
  }

  updateState(newState) {
    this.state = Object.assign(this.state, newState);
    this.emit(ADAPTER_STATE_CHANGE, this.state);
  }
}

module.exports = {
  ThresholdAdapterDriverFactory,
  ThresholdAdapterDriver,
  ThresholdAdapter,
};
