const fs = require('fs');

class FS {
  exists(file) {
    return fs.existsSync(file);
  }

  writeFile(file, data) {
    return fs.writeFileSync(file, data);
  }

  unlink(file) {
    return fs.unlinkSync(file);
  }

  readFile(file) {
    return fs.readFileSync(file);
  }
}

module.exports = new FS();
